const md5 = require('md5')
const connection = require('../config/database.js')
const userModel = require('../models/user')
const user = new userModel(connection)
const results = require('./results');
const result = new results

class Users {

    constructor(){}

    async register(req, res){
        let firstname = req.body.firstname
        let lastname = req.body.lastname
        let email = req.body.email
        let phone = req.body.phone
        let password = req.body.password
        password = md5(password)

        try {
            let checkEmail = await user.checkEmail(email)

            if (checkEmail.length > 0){
                res.send(result.errorResult("Email's been taken."))
                res.end()
            } else {
                try {
                    let add = await user.add(firstname, lastname, email, phone, password)

                    res.send(result.successResult("Data's been inserted.", add))
                    res.end()
                } catch (error){
                    console.log(error)
                    res.send(result.errorResult("Error has occured."))
                    res.end()
                }
            }
        }
        catch (error){
            console.log(error)
            res.send(result.errorResult("Error has occured."))
            res.end()
        }
    }

    async signIn(req, res){
        let email = req.body.email
        let password = req.body.password
        password = md5(password)

        try {
            let signIn = await user.signIn(email, password)

            if (signIn.length > 0) {
                if (signIn[0].password === password){
                    res.send(result.successResult("You're now sign in."))
                    res.end()
                }
            } else {
                res.send(result.errorResult("User's not found."))
                res.end()
            }
        } catch (error){
            console.log(error)
            res.send(result.errorResult("Error has occured."))
            res.end()
        }
    }
}

module.exports = Users