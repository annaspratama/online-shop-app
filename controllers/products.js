const connection = require('../config/database.js')
const productModel = require('../models/product')
const product = new productModel(connection)
const results = require('./results');
const result = new results

class Products {
    constructor(){}

    async getProduct(req, res){
        let productId = req.params.productId

        try {
            let productDetail = await product.productDetail(productId)
            
            res.send(result.successResult("Data's been retrieved.", productDetail[0]))
            res.end()
        } catch (error) {
            console.log(error)
            res.send(result.errorResult("Error has occured."))
            res.end()
        }
    }

    async getProducts(req, res){
        let min = req.body.min
        let max = req.body.max
        let sort = req.body.sort
        let limit = parseInt(req.body.limit)
        let start = parseInt(req.body.start)

        try {
            let getProducts = await product.getProducts(start, limit, min, max, sort)
            
            res.send({
                'error': false,
                'start': start + limit,
                'message': "Data's been retrieved.",
                'data': getProducts
            })
            res.end()
        } catch (error) {
            console.log(error)
            res.send(result.errorResult("Error has occured."))
            res.end()
        }
    }

    async getShoppingList(req, res){
        let userId = req.params.userId

        try {
            let getShoppingList = await product.getShoppingList(userId)
            
            res.send(result.successResult("Data's been retrieved.", getShoppingList))
            res.end()
        } catch (error) {
            console.log(error)
            res.send(result.errorResult("Error has occured."))
            res.end()
        }
    }

    async shoppingListCounter(req, res){
        let userId = req.params.userId

        try {
            let getShoppingList = await product.getShoppingList(userId)
            
            res.send(result.successResult("Data's been retrieved.", getShoppingList.length))
            res.end()
        } catch (error) {
            console.log(error)
            res.send(result.errorResult("Error has occured."))
            res.end()
        }
    }

    async getFlashsale(req, res){
        try {
            let flashsale = await product.getFlashsale()
            
            res.send(result.successResult("Data's been retrieved.", flashsale))
            res.end()
        } catch (error) {
            console.log(error)
            res.send(result.errorResult("Error has occured."))
            res.end()
        }
    }
}

module.exports = Products