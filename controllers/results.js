const error = {error: true}
const success = {error: false}

class Results {
    errorResult(message, data = null){
        error.message = message
        error.data = data

        return error
    }

    successResult(message, data = null){
        success.message = message
        success.data = data

        return success
    }
}

module.exports = Results