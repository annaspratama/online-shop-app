-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 16, 2020 at 04:45 AM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.3.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `online_shop_bdi`
--

-- --------------------------------------------------------

--
-- Table structure for table `flashsale`
--

CREATE TABLE `flashsale` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL COMMENT '0: Inactive, 1: Active'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `flashsale`
--

INSERT INTO `flashsale` (`id`, `product_id`, `status`) VALUES
(1, 2, 1),
(2, 3, 1);

-- --------------------------------------------------------

--
-- Table structure for table `histories`
--

CREATE TABLE `histories` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `total_product` int(11) NOT NULL DEFAULT '0',
  `price` decimal(15,2) NOT NULL DEFAULT '0.00',
  `discount` decimal(6,2) NOT NULL,
  `status` varchar(50) NOT NULL DEFAULT 'Unpaid',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `histories`
--

INSERT INTO `histories` (`id`, `user_id`, `product_id`, `total_product`, `price`, `discount`, `status`, `created_at`) VALUES
(1, 5, 1, 3, '45000.00', '0.00', 'Unpaid', '2020-03-10 07:03:04'),
(2, 5, 3, 2, '3200000.00', '30.00', 'Unpaid', '2020-03-10 07:30:41');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `price` decimal(15,2) DEFAULT NULL,
  `description` text,
  `images` text,
  `discount` decimal(6,2) DEFAULT NULL,
  `merk` varchar(100) DEFAULT NULL,
  `type` varchar(100) DEFAULT NULL,
  `size` varchar(50) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `name`, `price`, `description`, `images`, `discount`, `merk`, `type`, `size`, `created_at`) VALUES
(1, 'Kaos Polos', '45000.00', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer nec dignissim nulla. Praesent scelerisque euismod nisi ac accumsan. Morbi nec luctus purus, sit amet porta dolor. In sit amet nisl vitae est eleifend aliquam. Pellentesque maximus in nulla eget ullamcorper. Nullam aliquam dolor et lectus porttitor suscipit. Curabitur metus ligula, tristique a faucibus et, egestas ac risus. Quisque vel congue sem. Aenean dignissim justo non efficitur hendrerit. Morbi vestibulum felis dolor. Aliquam fringilla luctus urna, eget feugiat nunc pharetra eget. Fusce nec orci ut ipsum elementum aliquet. Praesent sit amet venenatis ante. Donec condimentum rhoncus euismod. Fusce mollis condimentum cursus.', 'http://localhost:7000/images/kaospolos.jpg', '0.00', 'Gildan', 'Combed 30s', 'S, M, L, XL, XXL', '2020-03-07 11:00:00'),
(2, 'Celana Chinos', '150000.00', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer nec dignissim nulla. Praesent scelerisque euismod nisi ac accumsan. Morbi nec luctus purus, sit amet porta dolor. In sit amet nisl vitae est eleifend aliquam. Pellentesque maximus in nulla eget ullamcorper. Nullam aliquam dolor et lectus porttitor suscipit. Curabitur metus ligula, tristique a faucibus et, egestas ac risus. Quisque vel congue sem. Aenean dignissim justo non efficitur hendrerit. Morbi vestibulum felis dolor. Aliquam fringilla luctus urna, eget feugiat nunc pharetra eget. Fusce nec orci ut ipsum elementum aliquet. Praesent sit amet venenatis ante. Donec condimentum rhoncus euismod. Fusce mollis condimentum cursus.', 'http://localhost:7000/images/celanashinos.jpg', '50.00', 'Chinos', 'Warna Cream', '28, 29, 30, 31, 32, 33, 34, 35', '2020-03-07 05:32:00'),
(3, 'Sepatu Adidas', '3200000.00', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer nec dignissim nulla. Praesent scelerisque euismod nisi ac accumsan. Morbi nec luctus purus, sit amet porta dolor. In sit amet nisl vitae est eleifend aliquam. Pellentesque maximus in nulla eget ullamcorper. Nullam aliquam dolor et lectus porttitor suscipit. Curabitur metus ligula, tristique a faucibus et, egestas ac risus. Quisque vel congue sem. Aenean dignissim justo non efficitur hendrerit. Morbi vestibulum felis dolor. Aliquam fringilla luctus urna, eget feugiat nunc pharetra eget. Fusce nec orci ut ipsum elementum aliquet. Praesent sit amet venenatis ante. Donec condimentum rhoncus euismod. Fusce mollis condimentum cursus.', 'http://localhost:7000/images/sepatuadidas.jpg', '30.00', 'Adidas', 'Warna Hitam', '39, 40, 41, 42, 43', '2020-03-05 00:13:17');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `firstname` varchar(100) DEFAULT NULL,
  `lastname` varchar(100) DEFAULT NULL,
  `email` varchar(80) NOT NULL,
  `phone` varchar(13) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `firstname`, `lastname`, `email`, `phone`, `password`) VALUES
(5, 'Tong', 'Wije', 'tong.wije@mail.com', '085123654888', '0192023a7bbd73250516f069df18b500'),
(6, 'Tong', 'Wije', 'tong.wije@mail.com', '085123654888', '0192023a7bbd73250516f069df18b500'),
(7, 'Tong', 'Wije', 'tong.wije2@mail.com', '085123654888', '0192023a7bbd73250516f069df18b500'),
(8, 'Tong', 'Wije', 'tong.wije3@mail.com', '085123654888', '0192023a7bbd73250516f069df18b500'),
(9, 'Tong', 'Wije', 'tong.wije4@mail.com', '085123654888', '0192023a7bbd73250516f069df18b500'),
(10, 'Tong', 'Wije', 'tong.wije5@mail.com', '085123654888', '0192023a7bbd73250516f069df18b500'),
(11, 'Tong', 'Wije', 'tong.wije6@mail.com', '085123654888', '0192023a7bbd73250516f069df18b500'),
(12, 'Tong', 'Wije', 'tong.wije7@mail.com', '085123654888', '0192023a7bbd73250516f069df18b500');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `flashsale`
--
ALTER TABLE `flashsale`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `histories`
--
ALTER TABLE `histories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `flashsale`
--
ALTER TABLE `flashsale`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `histories`
--
ALTER TABLE `histories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
