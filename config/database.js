const mysql = require("mysql");
const util  = require("util");

let pool = mysql.createPool({
    connectionLimit: 10,
    host: 'localhost',
    user: 'root',
    password: '',
    database: 'online_shop_bdi',
    dateStrings: true
  })
  
  pool.getConnection((err, connection) => {
    if (err) {
        if (err.code === "PROTOCOL_CONNECTION_LOST") {
            console.error("Database connection was closed.")
        } else if (err.code === "ER_CON_COUNT_ERROR") {
            console.error("Database has too many connections.")
        } else if (err.code === "ECONNREFUSED") {
            console.error("Database connection was refused.")
        }
    }
    
    if (connection) connection.release()
    
    return
  })
  
  pool.query = util.promisify(pool.query);
  
  module.exports = pool;