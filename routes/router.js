const express = require('express')
const router = express.Router()
const fetch = require('node-fetch')
const usersController = require('../controllers/users')
const users = new usersController
const productsController = require('../controllers/products')
const products = new productsController

// View routes
router.get('/', (req, res) => {
  fetch("http://" + req.get('host') + "/controller/product/detail/2", {'method': 'GET', /*'body': JSON.stringify({'id': 1})*/})
  .then(res => res.json())
  .then(data => {
    res.render('index', data)
  })
})

// Controller routes
router.post('/controller/user/register', users.register)
router.post('/controller/user/signin', users.signIn)
router.get('/controller/product/detail/:productId', products.getProduct)
router.post('/controller/product/get-all', products.getProducts)
router.get('/controller/product/shopping-list/:userId', products.getShoppingList)
router.get('/controller/product/shopping-counter/:userId', products.shoppingListCounter)
router.get('/controller/product/flashsale', products.getFlashsale)

module.exports = router;