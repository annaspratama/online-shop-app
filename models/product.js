class Product {
    constructor(conn){
        this.conn = conn
    }

    productDetail(productId){
        let data = new Promise((resolve, reject) => {
            this.conn.query('SELECT *, (price * ((100 - discount)/100)) total_price FROM products WHERE id = ? LIMIT 1',
            [productId],
            (error, data) => {
                if (error)
                    reject(error)
                else
                    resolve(data)
            })
        })

        return data
    }

    getProducts(start, limit, min, max, sort){
        let range = ''
        let order = ''

        // Set range
        if (min !== "" && max === ""){
            range = 'WHERE price >= '+ parseFloat(min)
        } else if (min === "" && max !== ""){
            range = 'WHERE price <= '+ parseFloat(max)
        } else if (min !== "" && max !== ""){
            range = 'WHERE price >= '+ parseFloat(min) +' AND price <= '+ parseFloat(max)
        }

        // Set sort
        switch (sort){
            case 'newest':
                order = 'created_at DESC'
                break
            case 'asc':
                order = 'name ASC'
                break
            case 'desc':
                order = 'name DESC'
                break
            default:
                order = 'name ASC'
        }

        let data = new Promise((resolve, reject) => {
            this.conn.query('SELECT *, (price * discount/100) discount_price FROM products '+ range +' ORDER BY '+ order +' LIMIT ?, ?',
            [start, limit],
            (error, data) => {
                if (error)
                    reject(error)
                else
                    resolve(data)
            })
        })

        return data
    }

    getShoppingList(userId){
        let data = new Promise((resolve, reject) => {
            this.conn.query('SELECT h.id history_id, p.id product_id, p.name product_name, h.price, h.discount, h.total_product, h.status, h.created_at, (h.total_product * h.price * ((100-h.discount)/100)) total_price FROM histories h INNER JOIN products p ON p.id = h.product_id WHERE h.user_id = ? ORDER BY created_at DESC',
            [userId],
            (error, data) => {
                if (error)
                    reject(error)
                else
                    resolve(data)
            })
        })

        return data
    }

    getFlashsale(){
        let data = new Promise((resolve, reject) => {
            this.conn.query('SELECT p.*, f.status FROM flashsale f INNER JOIN products p ON p.id = f.product_id WHERE status = 1 ORDER BY p.name ASC',
            [],
            (error, data) => {
                if (error)
                    reject(error)
                else
                    resolve(data)
            })
        })

        return data
    }
}

module.exports = Product