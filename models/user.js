class User {
    constructor(conn){
        this.conn = conn
    }

    checkEmailCb(email, callback){
        this.conn.query('SELECT id FROM users WHERE email = ?',
        [email],
        (error, data) => {
            if (error)
                return callback(error)
                
                return callback(null, data)
        })
    }

    checkEmail(email){
        let data = new Promise((resolve, reject) => {
            this.checkEmailCb(email, (error, data) => {
                if (error)
                    reject(error)
                else
                    resolve(data)
            })
        })

        return data
    }

    add(firstname, lastname, email, phone, password){
        let data = new Promise((resolve, reject) => {
            this.conn.query('INSERT INTO users (firstname, lastname, email, phone, password) VALUES (?, ?, ?, ?, ?)',
            [firstname, lastname, email, phone, password],
            (error, data) => {
                if (error)
                    reject(error)
                else
                    resolve(data)
            })
        })

        return data
    }

    signIn(email, password){
        let data = new Promise((resolve, reject) => {
            this.conn.query('SELECT * FROM users WHERE email = ? AND password = ? LIMIT 1',
            [email, password],
            (error, data) => {
                if (error)
                    reject(error)
                else
                    resolve(data)
            })
        })

        return data
    }
}

module.exports = User